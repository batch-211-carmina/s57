let collection = [];
// Write the queue functions below. 


//Gives all the elements of the queue
function print(){
    return(collection);
}

//Adds an element/elements to the end of the queue
function enqueue(item){
    
    // solution #1
    // collection.push(item);
    // return collection;

    // solution #2
    collection = [...collection, item];
    return collection;
}

//Removes an element in front of the queue
function dequeue(){

    // solution #1
    // collection.shift();
    // return collection;

    for (let i = 1; i < collection.length; i++) {
        collection[i - 1] = collection[i];
    }
    collection.length--;

    return collection;
}


//Shows the element at the front
function front(){

    //solution #1
    return collection[0];

    //solution #2
    // let first = collection.slice(0, 1).shift();
    // return first;

    //solution #3
    // let first = [...collection].shift();
    // return first;
}

//Shows the total number of elements
function size(){
     return collection.length; 
}

//Gives a Boolean value describing whether the queue is empty or not
function isEmpty(){
    return collection.length === 0;
}


// Export create queue functions below.
module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};